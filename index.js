var server   = require('./src/core/server'),
    mongoose = require('./src/core/db'),
    morgan   = require('morgan'),
    route    = require('./src/route');

// log
server.use(morgan('dev'));

// routing
route(server);

// Listening
server.listen(process.env.PORT || 8080, function () {
    console.log("Server listening @ ", process.env.PORT || 8080);
});
