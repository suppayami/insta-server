// dependencies
var config   = require('../config'),
    mongoose = require('mongoose');

// setup database
mongoose.connect(process.env.MONGOLAB_URI || config.mongodb.url);

module.exports = mongoose;
