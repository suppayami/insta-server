// dependencies
var restify = require('restify'),
    server  = restify.createServer();

// Accept parser
server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());
server.use(restify.fullResponse());

// CORS
server.use(function(req, res, next) {
    res.header('Access-Control-Allow-Headers', 'Content-Type, x-auth-token, X-Auth-Token');
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Origin', '*');
    next();
});

server.opts(/\.*/, function (req, res, next) {
    res.send(200);
    next();
});

module.exports = server;
