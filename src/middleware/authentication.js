var config = require('../config');
var jwt  = require('jsonwebtoken');
var User = require('../model/User');

module.exports = function(server) {
    server.use(function(req, res, next) {
        var token = req.headers['X-Auth-Token'] || req.headers['x-auth-token'];

        if ((req.url === "/api/user/authenticate" && req.method.toUpperCase() === "POST")
            || (req.url === "/api/user" && req.method.toUpperCase() === "POST")
            || (req.url === "/setup")) {
            return next();
        }

        if (!token) {
            res.send(401, {success: false, message: 'No token provided.'});
            return next();
        }

        jwt.verify(token, config.secret, function(err, decoded) {
            if (err) {
                res.send(403, {success: false, message: 'Failed to authenticate.'});
                return next();
            }

            User.findOne({_id:decoded._id,username:decoded.username}, function(err, user) {
                if (err) throw err;

                if (!!user) {
                    console.log(user);
                    req.currentUser = decoded;
                    return next();
                }

                res.send(403, {success: false, message: 'Failed to authenticate.'});
                next();
            });
        });
    });
};
