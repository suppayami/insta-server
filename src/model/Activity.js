var mongoose = require('../core/db');
var Schema = mongoose.Schema;
var activitySchema = require('../schema/Activity');

module.exports = mongoose.model('Activity', activitySchema);
