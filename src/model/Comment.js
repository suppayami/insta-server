var mongoose = require('../core/db');
var Schema = mongoose.Schema;
var commentSchema = require('../schema/Comment');

module.exports = mongoose.model('Comment', commentSchema);
