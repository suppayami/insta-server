var mongoose = require('../core/db');
var followSchema = require('../schema/Follow');

module.exports = mongoose.model('Follow', followSchema);
