var mongoose = require('../core/db');
var likeSchema = require('../schema/Like');

module.exports = mongoose.model('Like', likeSchema);
