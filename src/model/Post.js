var mongoose = require('../core/db');
var Schema = mongoose.Schema;
var postSchema = require('../schema/Post');

module.exports = mongoose.model('Post', postSchema);
