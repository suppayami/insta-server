var mongoose = require('../core/db');
var userSchema = require('../schema/User');

module.exports = mongoose.model('User', userSchema);
