module.exports = function(server) {
    // middleware
    require('./middleware/authentication')(server);

    // route
    require('./routes/TestRoutes')(server);
    require('./routes/UserRoutes')(server);
    require('./routes/PostRoutes')(server);
    require('./routes/CommentRoutes')(server);
    require('./routes/LikeRoutes')(server);
};
