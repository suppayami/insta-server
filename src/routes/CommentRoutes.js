var config = require('../config');
var jwt  = require('jsonwebtoken');
var Comment = require('../model/Comment');
var Post = require('../model/Post');
var Activity = require('../model/Activity');

module.exports = function(server) {
    server.get('/api/comment/post/:id', function(req, res, next) {
        Comment.find({post: req.params.id})
            .populate('user', 'username avatar')
            .sort({createdAt: -1})
            .exec(function(err, comments) {
                if (err) throw err;

                res.send(comments)
            });

        next();
    });

    server.post('/api/comment/post/:id', function(req, res, next) {
        var description = req.body.description,
            userId = req.currentUser._id,
            postId = req.params.id,
            newComment = new Comment({
                description: description,
                user: userId,
                post: postId
            });

        newComment.save(function(err, comment) {
            if (err) throw err;

            Post.update({_id: postId}, {$inc: {'commentCount': 1}}, function(err,res) {
                // do something here?!
            });

            Post.findOne({_id: req.params.id}, function(err, post) {
                if (err) throw err;

                newActivity = new Activity({
                    actor: req.currentUser._id,
                    actee: post.user,
                    post: req.params.id,
                    type: "comment"
                });

                newActivity.save(function(err) {
                    if (err) throw err;
                });
            });

            comment.populate({path: 'user', select: 'username avatar'}, function(err, cmt) {
                res.send({success: true, message: 'Commented successfully!', comment: cmt});
                next();
            });
        });
    });
};
