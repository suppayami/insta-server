var config = require('../config');
var jwt  = require('jsonwebtoken');
var Like = require('../model/Like');
var Post = require('../model/Post');
var Activity = require('../model/Activity');

module.exports = function(server) {
    server.get('/api/like/post/:id', function(req, res, next) {
        Like.find({likee: req.params.id}, 'liker')
            .populate('liker', 'username avatar')
            .exec(function(err, users) {
                if (err) throw err;

                res.send(users.map(function(value) {
                    return value.liker;
                }));

                next();
            });
    });

    server.put('/api/like/post/:id', function(req, res, next) {
        var newActivity;

        Like.findOne({
            liker: req.currentUser._id,
            likee: req.params.id
        }, function(err, like) {
            if (err) throw err;

            if (!!like) {
                like.remove(function(err) {
                    if (err) throw err;
                });

                res.send({success: true, message: 'Unlike successfully!'});

                Post.update({_id: req.params.id},
                            {$inc: {'likeCount': -1}, $pull: {'likes': req.currentUser._id}},
                            function(err,res) {
                                // do something here?!
                            });

                return next();
            }

            newLike = new Like({
                liker: req.currentUser._id,
                likee: req.params.id
            });

            newLike.save(function(err) {
                if (err) throw err;

                res.send({success: true, message: 'Like successfully!'});

                Post.update({_id: req.params.id},
                            {$inc: {'likeCount': 1}, $push: {'likes': req.currentUser._id}},
                            function(err,res) {
                    // do something here?!
                });

                Post.findOne({_id: req.params.id}, function(err, post) {
                    if (err) throw err;

                    newActivity = new Activity({
                        actor: req.currentUser._id,
                        actee: post.user,
                        post: req.params.id,
                        type: "like"
                    });

                    newActivity.save(function(err, activity) {
                        if (err) throw err;

                        console.log(activity);
                    });
                });

                next();
            });
        });
    });
};
