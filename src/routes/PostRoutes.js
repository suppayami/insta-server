var config = require('../config');
var jwt  = require('jsonwebtoken');
var User = require('../model/User');
var Post = require('../model/Post');
var Follow = require('../model/Follow');
var Like = require('../model/Like');
var Activity = require('../model/Activity');
var cloudinary = require('cloudinary');
var fs = require('fs');

module.exports = function(server) {
    // config cloudinary
    cloudinary.config(config.cloudinary);

    server.get('/api/post/all', function(req, res, next) {
        Post.find()
            .populate('user', 'username avatar')
            .sort({createdAt: -1})
            .exec(function(err, posts) {
                if (err) throw err;

                res.send(posts)
                next();
            });
    });

    server.get('/api/post', function(req, res, next) {
        Post.find({user: req.currentUser._id})
            .populate('user', 'username avatar')
            .sort({createdAt: -1})
            .exec(function(err, posts) {
                if (err) throw err;

                res.send(posts)
                next();
            });
    });

    server.post('/api/post', function(req, res, next) {
        var url,
            description = req.body.description,
            userId = req.currentUser._id,
            newPost,
            stream = cloudinary.uploader.upload(req.files.image.path, function(result) {
                url = result.url;
                newPost = new Post({
                    url: url,
                    description: description,
                    user: userId
                });

                newPost.save(function(err) {
                    if (err) throw err;

                    User.update({_id: userId}, {$inc: {'postCount': 1}}, function(err,res) {
                        // do something here?!
                    });

                    res.send({success: true, message: 'Posted successfully!', post: newPost});
                    next();
                });
            });
    });

    server.get('/api/post/feeds', function(req, res, next) {
        var followees;

        Follow.find({follower: req.currentUser._id}, 'followee', function(err, follows) {
            if (err) throw err;

            followees = follows.map(function(value) {
                return value.followee;
            });

            followees.push(req.currentUser._id);

            Post.find({user: {$in: followees}})
                .populate('user', 'username avatar')
                .sort({createdAt: -1})
                .exec(function(err, posts) {
                    if (err) throw err;

                    res.send(posts);
                    next();
                });
        });
    });

    server.get('/api/post/:id', function(req, res, next) {
        Post.findOne({_id: req.params.id})
            .populate('user', 'username avatar')
            .exec(function(err, post) {
                if (err) throw err;

                res.send(post)
                next();
            });
    });

    server.get('/api/post/user/:id', function(req, res, next) {
        Post.find({user: req.params.id})
            .populate('user', 'username avatar')
            .sort({createdAt: -1})
            .exec(function(err, posts) {
                if (err) throw err;

                res.send(posts)
                next();
            });
    });
};
