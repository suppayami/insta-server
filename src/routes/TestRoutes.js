var User = require('../model/User');
var Post = require('../model/Post');

module.exports = function(server) {
    server.get('/setup', function(req, res, next) {
        var yami = new User({
            username: "Dr.Yami",
            password: "password",
            avatar: "http://yami.moe/img/kudchan.jpg"
        });

        var chich = new User({
            username: "Dr.Chich",
            password: "password",
            avatar: "https://pbs.twimg.com/profile_images/645574130654273537/FEflxro4.png"
        });

        yami.save(function(err) {
            if (err) throw err;

            console.log(yami.username + ' Saved successfully!');

            var post = new Post({
                user: yami._id,
                url: 'https://farm6.staticflickr.com/5764/21156222148_fdf34e936e_k_d.jpg',
                description: "Test 1"
            });

            var post2 = new Post({
                user: yami._id,
                url: 'https://farm1.staticflickr.com/593/21781515970_a35f4be054_k_d.jpg',
                description: "Test 2"
            });

            post.save(function(err) {
                if (err) throw err;

                console.log(post.id + ' Post Saved successfully!');
            });

            post2.save(function(err) {
                if (err) throw err;

                console.log(post2.id + ' Post Saved successfully!');
            });
        });

        chich.save(function(err) {
            if (err) throw err;

            console.log(chich.username + ' Saved successfully!');

            var post = new Post({
                user: chich._id,
                url: 'https://farm6.staticflickr.com/5661/21451245496_23189193a4_k_d.jpg',
                description: "Test 3"
            });

            post.save(function(err) {
                if (err) throw err;

                console.log(post.id + ' Post Saved successfully!');
            });
        });

        res.send('saved successfully!');

        next();
    });
};
