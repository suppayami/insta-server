var config = require('../config');
var jwt  = require('jsonwebtoken');
var User = require('../model/User');
var Follow = require('../model/Follow');
var Activity = require('../model/Activity');
var cloudinary = require('cloudinary');

module.exports = function(server) {
    cloudinary.config(config.cloudinary);

    // testing purpose
    server.get('/api/user/all', function(req, res, next) {
        User.find({}, 'username avatar', function(err, users) {
            res.send(users);
        });

        next();
    });

    server.get('/api/user', function(req, res, next) {
        User.findOne({ _id: req.currentUser._id }, 'username avatar postCount followerCount followeeCount', function(err, user) {
            if (err) throw err;

            res.send(user);
        });

        next();
    });

    server.post('/api/user', function(req, res, next) {
        var username = req.body.username,
            password = req.body.password,
            newUser;

        if (username.length === 0 || password.length === 0) {
            res.send(403, {success: false, message: 'Username or password hasn\'t been input.'});
            return next();
        }

        User.findOne({username: username}, function(err, user) {
            if (err) throw err;

            if (!!user) {
                res.send(403, {success: false, message: 'Input username has been used.'});
                return next();
            }

            newUser = new User({
                username: username,
                password: password,
                avatar: ""
            });

            newUser.save(function(err) {
                if (err) throw err;

                res.send({success: true, message: 'Registered successfully!', username: username});
            });

            next();
        });
    });

    server.put('/api/user', function(req, res, next) {
        User.findOne({_id: req.currentUser._id}, function(err, user) {
            if (err) throw err;

            if (!user) {
                res.send({success: false, message: 'Update failed. User not found.'});
                return next();
            }

            if (!!req.body.newPassword && req.body.newPassword !== "") {
                if (user.password !== req.body.oldPassword) {
                    res.send({success: false, message: 'Authentication failed. Wrong password.'});
                    return next();
                }

                user.password = req.body.newPassword;
            }

            if (!!req.files.image) {
                cloudinary.uploader.upload(req.files.image.path, function(result) {
                    user.avatar = result.url;
                    user.save(function(err) {
                        if (err) throw err;

                        res.send({success: true, message: 'Update profile successfully!',
                                    user: {id: user._id, username: user.username, avatar: user.avatar}});

                        next();
                    });
                });
            }

            // user.save(function(err) {
            //     if (err) throw err;

            //     res.send({success: true, message: 'Update profile successfully!',
            //                 user: {id: user._id, username: user.username, avatar: user.avatar}});

            //     next();
            // });
        });
    });

    server.get('/api/user/activity', function(req, res, next) {
        Activity.find({actee: req.currentUser._id})
            .populate('actor', 'username avatar')
            .populate('actee', 'username avatar')
            .populate('post', 'url')
            .sort({createdAt: -1})
            .exec(function(err, activity) {
                if (err) throw err;

                res.send(activity);

                next();
            });
    });

    server.get('/api/user/followers/:id', function(req, res, next) {
        Follow.find({followee: req.params.id}, 'follower')
            .populate('follower', 'username avatar postCount followerCount followeeCount')
            .exec(function(err, users) {
                if (err) throw err;

                res.send(users.map(function(value) {
                    return value.follower;
                }));

                next();
            });
    });

    server.get('/api/user/followees/:id', function(req, res, next) {
        Follow.find({follower: req.params.id}, 'followee')
            .populate('followee', 'username avatar postCount followerCount followeeCount')
            .exec(function(err, users) {
                if (err) throw err;

                res.send(users.map(function(value) {
                    return value.followee;
                }));

                next();
            });
    });

    server.get('/api/user/authenticate', function(req, res, next) {
        res.send({success: true, message: 'Is authenticated!', user: req.currentUser});
    });

    server.post('/api/user/authenticate', function(req, res, next) {
        User.findOne({username: req.body.username}, function(err, user) {
            var token;

            if (err) throw err;

            if (!user) {
                res.send(401, {success: false, message: 'User not found.'});
                return next();
            }

            if (user.password !== req.body.password) {
                res.send(401, {success: false, message: 'Wrong password.'});
                return next();
            }

            token = jwt.sign(
                        {_id: user._id, username: user.username, avatar: user.avatar},
                        config.secret,
                        {
                            expiresInMinutes: 1440
                        }
                    );

            res.send({success: true, message: 'Authentication success!', token: token,
                        user: {_id: user._id, username: user.username, avatar: user.avatar}});

            next();
        });
    });

    server.post('/api/user/search', function(req, res, next) {
        User.find({ $text : { $search : req.body.searchKey } })
            .select('username avatar postCount followerCount followeeCount')
            .exec(function(err, users) {
            if (err) throw err;

            if (!users) {
                res.send({success: false, message: 'User not found.'});
                return next();
            }

            res.send({success: true, message: 'Successfully searched users!', users: users});
            next();
        });
    });

    server.get('/api/user/follow/:id', function(req, res, next) {
        Follow.findOne({
            follower: req.currentUser._id,
            followee: req.params.id
        }, function(err, follow) {
            if (err) throw err;

            res.send({success: true, message: 'Check follow!', isFollow: !!follow});
            next();
        });
    });

    server.put('/api/user/follow/:id', function(req, res, next) {
        var newFollow;
        var newActivity;

        Follow.findOne({
            follower: req.currentUser._id,
            followee: req.params.id
        }, function(err, follow) {
            if (err) throw err;

            if (!!follow) {
                follow.remove(function(err) {
                    if (err) throw err;
                });

                User.update({_id: req.currentUser._id}, {$inc: {'followeeCount': -1}}, function(err,res) {
                    // do something here?!
                });

                User.update({_id: req.params.id}, {$inc: {'followerCount': -1}}, function(err,res) {
                    // do something here?!
                });

                res.send({success: true, message: 'Unfollow successfully!'});
                return next();
            }

            newFollow = new Follow({
                follower: req.currentUser._id,
                followee: req.params.id
            });

            newFollow.save(function(err) {
                if (err) throw err;

                User.update({_id: req.currentUser._id}, {$inc: {'followeeCount': 1}}, function(err,res) {
                    // do something here?!
                });

                User.update({_id: req.params.id}, {$inc: {'followerCount': 1}}, function(err,res) {
                    // do something here?!
                });

                newActivity = new Activity({
                    actor: req.currentUser._id,
                    actee: req.params.id,
                    type: "follow"
                });

                newActivity.save(function(err) {
                    if (err) throw err;
                });

                res.send({success: true, message: 'Follow successfully!'});
                next();
            });
        });
    });

    server.get('/api/user/:id', function(req, res, next) {
        var id = req.params.id;

        User.findOne({_id: id}, function(err, user) {
            if (err) throw err;

            if (!user) {
                res.send({success: false, message: 'User not found.'});
                return next();
            }

            res.send({success: true, message: 'Successfully get user profile!',
                        user: {_id: user._id, username: user.username, avatar: user.avatar,
                                postCount: user.postCount, followerCount: user.followerCount, followeeCount: user.followeeCount}});

            next();
        });
    });
};
