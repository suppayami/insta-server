var mongoose = require('../core/db');
var Schema = mongoose.Schema;

var schema = new Schema({
    type: String,
    actor: { type: Schema.Types.ObjectId, ref: 'User' },
    actee: { type: Schema.Types.ObjectId, ref: 'User' },
    post: { type: Schema.Types.ObjectId, ref: 'Post' },
    createdAt: { type: Date, default: Date.now }
});

module.exports = schema;
