var mongoose = require('../core/db');
var Schema = mongoose.Schema;

module.exports = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    post: { type: Schema.Types.ObjectId, ref: 'Post' },
    description: String,
    createdAt: { type: Date, default: Date.now }
});
