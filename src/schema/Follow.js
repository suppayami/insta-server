var mongoose = require('../core/db');
var Schema = mongoose.Schema;

var schema = new Schema({
    follower: { type: Schema.Types.ObjectId, ref: 'User' },
    followee: { type: Schema.Types.ObjectId, ref: 'User' }
});

schema.index({follower: 1, followee: 1});

module.exports = schema;
