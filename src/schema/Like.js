var mongoose = require('../core/db');
var Schema = mongoose.Schema;

var schema = new Schema({
    liker: { type: Schema.Types.ObjectId, ref: 'User' },
    likee: { type: Schema.Types.ObjectId, ref: 'Post' }
});

schema.index({liker: 1, likee: 1});

module.exports = schema;
