var mongoose = require('../core/db');
var Schema = mongoose.Schema;

module.exports = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    url: String,
    description: String,
    commentCount: {type: Number, default: 0},
    likeCount: {type: Number, default: 0},
    likes: [{ type: Schema.Types.ObjectId, ref: 'User' }],
    createdAt: { type: Date, default: Date.now }
});
