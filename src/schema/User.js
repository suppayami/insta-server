var mongoose = require('../core/db');
var Schema = mongoose.Schema;

var schema = new Schema({
    username: String,
    password: String,
    avatar: {type: String, default: "http://img2.wikia.nocookie.net/__cb20140823100428/micronations/images/4/41/No-Avatar-High-Definition.jpg"},
    postCount: {type: Number, default: 0},
    followerCount: {type: Number, default: 0},
    followeeCount: {type: Number, default: 0}
});

schema.index({username: 'text'});

module.exports = schema;
